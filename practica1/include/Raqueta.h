// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
//Fichero modificado por Pablo Martinez Campos (PabloM10)

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
