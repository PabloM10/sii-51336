// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//Fichero modificado por Pablo Martinez Campos (PabloM10)

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include "Socket.h"
#include <vector>
#include "Plano.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//Se añaden includes necesarios para el uso de la memoria compartida
#include "DatosMemCompartida.h" //Aqui ya se encuentra Esfera.h y Raqueta.h, por lo que podemos eliminar los includes
#include <sys/mman.h>


class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fd_logger;
	int fd;
	//int fd_secl; //Descriptor de fichero de la tubería FIFO Servidor-Cliente
	int fd_clse; //Descriptor de fichero de la tubería FIFO Cliente-Servidor
	int TUBERIA_CREADA;

	pthread_t thread;

	/*//Se añade atributo memoria compartida
	DatosMemCompartida *punt_bot;
	DatosMemCompartida bot; 
	char *proyeccion;*/

	Socket socket_conexclient;
	Socket socket_comclient;

	int cont=250; //Se añade contador para la funcionalidad de automatizar raqueta 2 si no se pulsa ninguna tecla en 10 segundos
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
