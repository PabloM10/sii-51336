// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	char salida[100];

	sprintf(salida, "---FIN---");
/*
	//Cerramos la FIFO Servidor-Cliente
	write(fd_secl, salida, strlen(salida)+1);	
	close(fd_secl);
	unlink("/tmp/FIFOservidor");

	//Cerramos la FIFO Cliente-Servidor
	write(fd_clse, salida, strlen(salida)+1);
	close (fd_clse);
	unlink ("/tmp/FIFOcliente");
*/	
	munmap(proyeccion, sizeof(bot)); //Se cierra la proyeccion de memoria

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	//Se añade código necesario para el Bot
	punt_bot->esfera=esfera;
	punt_bot->raqueta1=jugador1;
	punt_bot->raqueta2=jugador2;

	if (punt_bot->accion1==1) OnKeyboardDown('w',0,0); //Se mueve arriba
	else if (punt_bot->accion1==-1) OnKeyboardDown('s',0,0); //Se mueve abajo
	else if (punt_bot->accion1==0); //No hace nada

	if (punt_bot->accion2==1) OnKeyboardDown('o',0,0); //Se mueve arriba
	else if (punt_bot->accion2==-1) OnKeyboardDown('l',0,0); //Se mueve abajo
	else if (punt_bot->accion2==0); //No hace nada	

	char coord[100];
	socket_comserver.Receive(coord, sizeof(coord));

	sscanf(coord,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);

	//La partida acaba cuando uno de los jugadores alcance 3 puntos
	
	char cadena1[100];
	
	if ((puntos1==3) || (puntos2==3)) exit(1);

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[150];
	sprintf(cad, "%c", key);
	
	socket_comserver.Send(cad,sizeof(cad));
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

/*	
	//Se crea la tuberia Servidor-Cliente en modo lectura
	unlink("/tmp/FIFOservidor");

	int fd1;
	fd1=mkfifo("/tmp/FIFOservidor", 0666);
	if (fd1==-1) perror("Error en la creacion de la FIFO Servidor-Cliente");
	else printf ("Creacion correcta de la FIFO Servidor-Cliente");

	//Se abre la tuberia Servidor-Cliente
	fd_secl=open("/tmp/FIFOservidor", O_RDONLY);
	if (fd_secl==-1) perror("Error en la apertura de la FIFO Servidor-Cliente"); 
	else printf ("Se ha abierto correctamente la FIFO Servidor-Cliente");

	//Se crea la tuberia Cliente-Servidor
	unlink("/tmp/FIFOcliente");

	int fd2;
	fd1=mkfifo("/tmp/FIFOcliente", 0666);
	if (fd2==-1) perror("Error en la creacion de la FIFO Cliente-Servidor");
	else printf ("Creacion correcta de la FIFO Cliente-Servidor");	

	//Se abre la tuberia Servidor-Cliente
	fd_clse=open("/tmp/FIFOcliente", O_WRONLY);
	if (fd_clse==-1) perror("Error en la apertura de la FIFO Cliente-Servidor"); 

*/	
	//Codigo necesario para el Bot

	fd=open("/tmp/MemCompartida", O_RDWR|O_CREAT|O_TRUNC, 0666);
	if (fd==-1) {
		printf ("Error apertura del fich donde vamos a proyectar \n");
		close(fd);
		exit(1);
	}

	write (fd, &bot, sizeof(bot)); //Se escribe en el fichero
	
	proyeccion=(char*)mmap(NULL,sizeof(bot),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0); //Se asigna al puntero de proyeccion el lugar donde se encuentra proyectado el fichero

	if (proyeccion==MAP_FAILED){
		perror("Error al abrir el fichero de proyeccion");
		exit(1);
	}	

	close(fd); //Se cierra el fichero
	punt_bot=(DatosMemCompartida*)proyeccion; //Se asigna el valor del puntero de proyeccion al puntero de memoria compartida
	punt_bot->accion1=0; //No se quiere que de inicio se realice ninguna accion
	punt_bot->accion2=0;

	//Codigo de los sockets
	char name[200];
	printf ("Introduzca nombre: ");
	scanf("%S", name);

	socket_comserver.Connect((char *) "127.0.0.1",2500);
	socket_comserver.Send(name,sizeof(name));
}
