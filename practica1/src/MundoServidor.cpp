// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>

void CMundoServidor::RecibeComandosJugador() {
	printf ("Entro al RecibeComandosJugador\n");	
	int cont=1;
	while(cont==1) {
		usleep(10);
		char cad[100];
		
		socket_comclient.Receive(cad,sizeof(cad));		

		unsigned char key;
		sscanf(cad,"%c",&key);
		
		if (cad[0]=='-'){
			cont=0;
		}
		
		else {
			if(key=='s')jugador1.velocidad.y=-4;
			if(key=='w')jugador1.velocidad.y=4;
			if(key=='l')jugador2.velocidad.y=-4;
			if(key=='o')jugador2.velocidad.y=4;
		}
	}
}

void* hilo_comandos (void* d){
		CMundoServidor* p=(CMundoServidor*) d;
		p->RecibeComandosJugador();
	}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	//Cerramos FIFO

	char salir[100];
	sprintf (salir, "---FIN---");

	write(fd_logger,salir,strlen(salir)+1);
	if (close(fd_logger)==-1)
	{
		perror ("ERROR AL CERRAR");
		exit(1);
	}

}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	cont--; //Se va decrementando el contador cada segundo

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		//Mensaje de la tuberia (al usar wirte, se guarda en el buffer cadena)
		 char cad[200];
 		sprintf(cad,"Jugador 2 marca 1 punto, lleva %d", puntos2);
 		write(fd,cad,strlen(cad)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//Mensaje de la tuberia (al usar write, se guarda en el buffer cadena)
		char cad[200];
 		sprintf(cad,"Jugador 1 marca 1 punto, lleva %d", puntos1);
 		write(fd,cad,strlen(cad)+1);
	}


	//Cad texto con datos a enviar
	char cad [200];
	sprintf (cad, "%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x, esfera.centro.y, jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2);


	socket_comclient.Send(cad,strlen(cad)+1);

	//El juego finaliza cuando uno de los dos jugadores anota 3 puntos
	if ((puntos1==3) || (puntos2==3)) exit(0);
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

/*	
	//Abrir la tubería en modo escritura
	fd_logger=open("/tmp/loggerfifo",O_WRONLY);
	if (fd_logger==-1)
	{
		perror("Error al abrir fifo_logger");
		exit(1);
	}

	//Abrir FIFO Servidor-Cliente
	fd_secl=open("/tmp/FIFOservidor",O_WRONLY);
	if (fd_secl==-1)
	{
		perror("Error al abrir FIFO Servidor-Cliente");
		exit(1);
	}

	//Abrir FIFO Cliente-Servidor
	fd_clse=open("/tmp/FIFOcliente", O_RDONLY);
	if (fd_clse==-1) {
		perror ("Error al abrir FIFO Cliente-Servidor");
		exit(1);
	}
*/
	//se crea el thread
	pthread_create(&thread,NULL,hilo_comandos,this);

	//Codigo Sockets
	
	socket_conexclient.InitServer((char *) "127.0.0.1", 2500);
	socket_comclient=socket_conexclient.Accept();
	char name[200];
	socket_comclient.Receive(name,sizeof(name));
	printf("Nombre del cliente en conexion: \n %s", name);
}
