#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h" //Aqui se encuentran Esfera.h y Raqueta.h

int main() {

	int fich;
	DatosMemCompartida *punt_bot;
	char *proyeccion;

	//Se abre el fichero creado en Mundo.cpp
	fich=open("/tmp/MemCompartida",O_RDWR); //No son necesarios los bits de permiso ya que solamente estamos abriendo el fichero

	//En caso de error en la apertura
	if (fich==-1) { 
		perror ("Error abriendo fichero");
		close(fich);
		return -1;
	}

	//Se proyecta el fichero
	punt_bot=(DatosMemCompartida*)mmap(NULL,sizeof(*(punt_bot)),PROT_WRITE|PROT_READ,MAP_SHARED,fich,0);
	
	if (punt_bot==MAP_FAILED) { //en caso de error
		perror ("No se puede abrir el fichero proyectado");
		return -1;
	}

	close (fich); //Cerramos descriptor de fichero

	//Control automatico de las raquetas
	while (1) {
		float posRaq1;
		float posRaq2;
		posRaq1=((punt_bot->raqueta1.y1+punt_bot->raqueta1.y2)/2);
		posRaq2=((punt_bot->raqueta2.y1+punt_bot->raqueta2.y2)/2);
		if (posRaq1<punt_bot->esfera.centro.y) punt_bot->accion1=1; //La raqueta 1 sube
		else if (posRaq1>punt_bot->esfera.centro.y) punt_bot->accion1=-1; //La raqueta 1 baja
		else punt_bot->accion1=0; //La raqueta 1 no se mueve

		if (posRaq2<punt_bot->esfera.centro.y) punt_bot->accion2=1; //La raqueta 2 sube
		else if (posRaq2>punt_bot->esfera.centro.y) punt_bot->accion2=-1; //La raqueta 2 baja
		else punt_bot->accion2=0; //La raqueta 2 no se mueve

		usleep(25000);
	}

	//Se desmonta la proyeccion de memoria
	munmap(proyeccion, sizeof(*(punt_bot)));

	return 1;
}		
		 
