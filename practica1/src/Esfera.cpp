// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	incremento=0.002f;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x=centro.x+velocidad.x*t; // Coordenada x del centro de la esfera que hay que mover
	centro.y=centro.y+velocidad.y*t; // Coordenada y del centro de las esfera que hay que mover

	radio-=incremento; //Disminuye progresivamente el radio de la esfera.

	if (radio>=0.5f || radio<=0.05f) incremento=-incremento; //Cuando llega a un minimo, vuelve a aumentar el radio hasta llegar a su tamaño original, y asi sucesivamente
	
}
