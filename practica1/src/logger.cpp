//logger.cpp: Este programa se encarga de crear la tubería (mkfifo), abrirla en modo lectura (open) y destruirla (close y unlink). Por lo tanto, el logger debe ser ejecutado antes que el tenis.

 #include <stdio.h>
 #include <sys/stat.h>
 #include <sys/types.h>
 #include <unistd.h>
 #include <fcntl.h>
 #include <stdlib.h>

 int main(int argc, char *argv[])
 {

	//Creacion tuberia
	unlink("/tmp/loggerfifo");

 	if(mkfifo("/tmp/loggerfifo", 0777)==-1) //Entra si hay error en la creacion
 	{
 		printf("Error al crear FIFO del logger");
		exit(1);
 	}

	else
	{
		printf ("FIFO logger creada");
	}

 	int fd=open("/tmp/loggerfifo",O_RDONLY); //Se abre la FIFO en modo lectura

 	if(fd==-1)
 	{	
 		perror("Error abriendo FIFO del logger");
 		exit(1);
 	}

	else printf ("FIFO del logger abierta");

 	char buff[250];
	int numLeido;
 	
	while(1)
 	{
 		numLeido=read(fd,buff,sizeof(buff)); // En caso de no dar error, se leen los datos (modificados en mundo) e imprimimos mensaje
		if (numLeido==-1) {
			perror("Error al leer FIFO del logger");
			exit(1);
		}

		else if (buff[0]=='-'){
			printf ("%s\n", buff);
			break;
		}
		
		else printf ("%s\n", buff);

 	}

 	close(fd); //Cerramos descriptor fifo
 	unlink("/tmp/loggerfifo"); //Se borra FIFO
 	return 0;
 }
