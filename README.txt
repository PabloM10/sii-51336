Instrucciones juego del tenis:

Para controlar la raqueta de la izquierda (jugador 1) se utiliza la tecla 'w' (para mover la raqueta hacia arriba) y la tecla 's' (para moverla hacia abajo). 

El mismo funcionamiento existe para la raqueta de la derecha (jugador 2): tecla 'o' para moverla hacia arriba y tecla 'l' para moverla hacia abajo.
